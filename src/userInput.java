//NAME: Chris Hinson
//Pitt Program one
//10/9/18

//This class handles all file input

import java.util.Scanner;

public class userInput {

    //This function is called any time we need to know if there's still a customer to be waited on
    //So it should execute very time a customer finishes and checks out
    public Boolean customerInLine()
    {

        //scanner for user input
        Scanner k = new Scanner(System.in);

        //return value
        Boolean isThereACustomer = null;

        //Input validation loop variable
        Boolean inputValidated = Boolean.FALSE;

        //Asks the user for their input
        System.out.println("Is there a customer in line? (Y/N)");
        //gets the user's next input
        String input = k.nextLine();

        do
        {

            //try catch in case any strange values get passed that breaks things
            try {

                //Checks for valid input
                //May be either 'Y','y','N', or 'n'
                if (validate.validString(input, "(?i)[yn]")) {
                    //checks if the user has inputted a "yes"
                    if (input.equals("y")||input.equals("Y"))
                    {
                        //acknowledge the user has told the program there is a nother customer
                        System.out.println("Excellent, waiting on next customer now");
                        //sets the return value to true
                        isThereACustomer = Boolean.TRUE;
                    }
                    else
                    {
                        //if the user has not input a "yes" value, sets the return variable to false which will exit the
                        //main loop
                        System.out.println("Alright, exiting program now");
                        isThereACustomer = Boolean.FALSE;
                    }
                    //breaks the input validation loop
                    inputValidated = Boolean.TRUE;
                }
                else
                {
                    //if a valid input has not been enetered, let the user know, and get the next input
                    System.out.println("Sorry, please enter a valid input");
                    input = k.nextLine();
                }
            }
            //just here to catch any weird inputs
            catch (Exception e)
            {
                System.out.println("That's not a valid input, please try again");
                input = k.nextLine();
            }
        }
        //only execute this loop while input has not been validated
        while (!inputValidated);

        //return the return variable
        return isThereACustomer;
    }

    public String getCustomerName()
    {
        Scanner k = new Scanner(System.in);
        String customerName = null;
        Boolean validated = Boolean.FALSE;

        System.out.println("What is the customer's name?");
        do
        {
            try {
                customerName = k.nextLine();
                System.out.println("Excellent, waiting on " + customerName + " now");
                validated = Boolean.TRUE;
            }
            catch (Exception e)
            {
                System.out.println("Sorry, that's not a valid input, please try again");
            }
        }
        while (!validated);

        return customerName;
    }

    public Boolean getDAMember (String customerName)
    {
        Boolean isDAMember= Boolean.FALSE;
        Boolean validated = Boolean.FALSE;
        Scanner k = new Scanner(System.in);
        int attemptsLeft = 3;


        String actualPasscode = "password";
        String userPasscode;

        //TODO:Please make this nicer.  Ask the customer if they are a meber, then get their passcode
        System.out.println("Hello " + customerName + ". Please enter your HoneyDukes password if you have a membership with us.");
        System.out.println("If you are not a member with us, please enter \"No\"");

        do
        {
            userPasscode = k.nextLine();

            if (userPasscode.equals(actualPasscode))
            {
                System.out.println("Thank you for being a HoneyDuke's club member "  + customerName);
                isDAMember = Boolean.TRUE;
                validated = Boolean.TRUE;
            }
            else if (validate.validString(userPasscode,("[Nn]o")))
            {
                System.out.println("Ok, you may begin shopping now.");
                validated = Boolean.TRUE;
            }
            else
            {
                attemptsLeft--;
                System.out.println("Sorry, that password is incorrect, you have " + (attemptsLeft) + " attempts left.");
            }
        }
        while(!validated && attemptsLeft !=0);

        return isDAMember;
    }

    public MenuItem addItemMenu(Boolean isDAMember)
    {
        Scanner k = new Scanner(System.in);
        Menu menu = new Menu();
        MenuItem MenuItemtoReturn = null;
        int itemChosen ;
        int numofItem;
        Boolean itemAdded = Boolean.FALSE;

        //TODO: This is a super inelegenat way to do this. Plz fix
        do {
            if (isDAMember == Boolean.TRUE) {
                menu.printDA();
                if (k.hasNextInt())
                {
                    itemChosen = k.nextInt();

                    switch (itemChosen)
                    {
                        case 1:MenuItemtoReturn = menu.getDAacidPop();
                            itemAdded = Boolean.TRUE;
                        break;
                        case 2:MenuItemtoReturn = menu.getDAeveryFlavoredBeansLarge();
                            itemAdded = Boolean.TRUE;
                        break;
                        case 3:MenuItemtoReturn = menu.getDAeveryFlavoredBeansSmall();
                            itemAdded = Boolean.TRUE;
                        break;
                        case 4:MenuItemtoReturn = menu.getDApumpkinPasties();
                            itemAdded = Boolean.TRUE;
                        break;
                        default: System.out.println("Please enter a number 1-4");

                    }
                }
                else
                {
                    System.out.println("That's not a valid option please try again");
                    k.next();
                }
            } else {
                menu.printReg();
                if (k.hasNextInt())
                {
                    itemChosen = k.nextInt();

                    switch (itemChosen)
                    {
                        case 1:MenuItemtoReturn = menu.getAcidPop();
                            itemAdded = Boolean.TRUE;
                        break;
                        case 2:MenuItemtoReturn = menu.getEveryFlavoredBeansLarge();
                            itemAdded = Boolean.TRUE;
                        break;
                        case 3:MenuItemtoReturn = menu.getEveryFlavoredBeansSmall();
                            itemAdded = Boolean.TRUE;
                        break;
                        case 4:MenuItemtoReturn = menu.getPumpkinPasties();
                            itemAdded = Boolean.TRUE;
                        break;
                        default: System.out.println("Please enter a number 1-4");

                    }


                }
                else
                {
                    System.out.println("That's not a valid option please try again");
                    k.next();
                }
            }

        }
        while (itemAdded == Boolean.FALSE);

        return MenuItemtoReturn;
    }

    public Integer getNumOfItemtoAdd(MenuItem item)
    {
        Boolean validated = Boolean.FALSE;
        Scanner k = new Scanner(System.in);
        Integer numOfItem = null;
        do {
            System.out.println("How many of this item would you like to add?");

            if (k.hasNextInt()) {
                numOfItem = k.nextInt();

                if (numOfItem<= 0)
                {
                    System.out.println("You cannot add 0 or negative amount of items");
                }
                else {
                    validated = Boolean.TRUE;
                }
            }
            else
            {
                System.out.println("That's not a valid option please try again");
                k.next();
            }
        }
        while(!validated);
        return numOfItem;
    }

    public Integer removeBillItem(Bill customerBill)
    {

        Integer billItemToRemove = null;
        Scanner k = new Scanner(System.in);

        Boolean validated = Boolean.FALSE;

        do
        {
            customerBill.print();
            System.out.println("Please enter the number of the item you'd like to remove");

            if (k.hasNextInt())
            {
                billItemToRemove = k.nextInt();
                if (billItemToRemove<customerBill.getItems().size()+1 && billItemToRemove>0)
                {
                    validated =Boolean.TRUE;
                }
                else
                {
                    billItemToRemove = null;
                    System.out.println("Please select an item number between 0 and " + customerBill.getItems().size());
                }

            }
            else
            {
                System.out.println("Please enter the NUMBER of the item you'd like to remove.");
                k.next();
            }
        }
        while(!validated);

        return billItemToRemove;
    }

    public Integer  getNumToRemove(Bill customerBill, int itemIndex)
    {
        Scanner k = new Scanner(System.in);
        Integer numOfItemToRemove = null;
        Boolean validated = Boolean.FALSE;

        System.out.println("How many of this item would you like to remove?");

        do
        {
            if (k.hasNextInt())
            {
                numOfItemToRemove = k.nextInt();
                if (numOfItemToRemove <= customerBill.getNumOfItems().get(itemIndex-1)&& numOfItemToRemove >=0)
                {
                    validated =Boolean.TRUE;
                }
                else
                {
                    numOfItemToRemove = null;
                    System.out.println("You may only remove as many items as you have in your cart.");
                }

            }
            else
            {
                System.out.println("Pleas enter how many of the item you'd like to remove");
                k.next();
            }
        }
        while (!validated);

        return numOfItemToRemove;
    }

    public int getNumOfCurrency ()
    {
        Scanner k = new Scanner(System.in);
        System.out.println("And how many of that would you like to pay at this time?");
        Boolean validated = Boolean.FALSE;
        int amount = 0;

        do
        {
            if (k.hasNextInt())
            {
                amount = k.nextInt();
                if (amount <=0)
                {
                    System.out.println("You cannot pay 0 or less than 0 please try again");
                }
                else
                {
                    validated = Boolean.TRUE;
                }
            }
            else
            {
                System.out.println("Please enter a the NUMBER of coins you would like to enter at this time.");
                k.next();
            }
        }while (!validated);

        return amount;

    }

    public int pay(int totalOwed)
    {

        Scanner k = new Scanner(System.in);
        int totalPayed= 0;

        do
        {
            System.out.println("Enter the currency type (Galleons, Sickles, or Knuts)");

            if (k.hasNext())
            {
                String currency = k.next();

                if (validate.validString(currency, "[Gg]alleons?")) {
                    int paymentAmount = getNumOfCurrency() * 493;
                    totalPayed = totalPayed + paymentAmount;

                    System.out.println("You have payed " + paymentAmount + " knuts");
                    if (totalPayed<totalOwed) {
                        System.out.println("You still owe " + (totalOwed - totalPayed));
                    }
                }
                else if (validate.validString(currency, "[Ss]ickles?"))
                {
                    int paymentAmount = getNumOfCurrency()*29;
                    totalPayed = totalPayed + paymentAmount;

                    System.out.println("You have payed " + paymentAmount + " knuts?");
                    if (totalPayed<totalOwed) {
                        System.out.println("You still owe " + (totalOwed - totalPayed));
                    }
                }
                else if (validate.validString(currency, "[Kk]nuts?"))
                {
                    int paymentAmount = getNumOfCurrency();
                    totalPayed = totalPayed + paymentAmount;

                    System.out.println("You have payed " + paymentAmount + " knuts");
                    if (totalPayed<totalOwed) {
                        System.out.println("You still owe " + (totalOwed - totalPayed));
                    }
                }
                else
                {
                    System.out.println("Sorry, that's not a valid currency type, you must enter Galleon, Sickle, or Knut");
                }

            }
            else
            {
                System.out.println("Please enter the type of currency you would like to enter you must enter Galleon, Sickle, or Knut");
            }

        }while (totalPayed<totalOwed);

        return totalPayed;
    }
}
