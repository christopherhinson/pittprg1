//NAME: Chris Hinson
//Pitt Program one
//10/9/18

//This class is the definition of a bill

import java.util.ArrayList;

public class Bill {

    //TODO: This could be converted to a single ArrayList of type MenuItem
    private ArrayList<String> items = new ArrayList<String>();
    private ArrayList<Integer> numOfItems = new ArrayList<Integer>();
    private ArrayList<Integer> costs = new ArrayList<Integer>();

    public Bill()
    {

    }


    public ArrayList<String> getItems() {
        return items;
    }

    public ArrayList<Integer> getNumOfItems()
    {
        return numOfItems;
    }

    public ArrayList<Integer> getCosts() {
        return costs;
    }

    public Boolean isEmpty()
    {
        if (items.isEmpty())
        {
            return Boolean.TRUE;
        }
        else
        {
            return Boolean.FALSE;
        }
    }

    public void addItem (MenuItem item, int numOfItem)
    {
        if (items.contains(item.getName()))
        {
            int index = items.indexOf(item.getName());
            int newNumber = numOfItems.get(index)+numOfItem;
            numOfItems.set(index, newNumber);
        }
        else
        {
            items.add(item.getName());
            costs.add(item.getCost());
            numOfItems.add(numOfItem);
        }
    }

    public void removeItem (int itemNumberIndex, int numOfItemToRemove)
    {
        itemNumberIndex = itemNumberIndex-1;
        if (numOfItemToRemove>= numOfItems.get(itemNumberIndex)) {
            items.remove(itemNumberIndex);
            items.trimToSize();

            numOfItems.remove(itemNumberIndex);
            numOfItems.trimToSize();

            costs.remove(itemNumberIndex);
            costs.trimToSize();
        }
        else
        {
            numOfItems.set(itemNumberIndex, (numOfItems.get(itemNumberIndex)-numOfItemToRemove));
        }
    }

    public void print()
    {
        for (int i = 0;i<items.size();i++)
        {
            System.out.println((i+1) + ") " + numOfItems.get(i) + " " + items.get(i) + " at " + (costs.get(i)) + " knuts each");
        }
    }

    public int getTotalOwed(Boolean DAMember)
    {
        int total = 0;
        for (int i = 0; i < items.size();i++)
        {
            total = total + (costs.get(i)*numOfItems.get(i));
        }

        if (DAMember ==Boolean.TRUE && total>290)
        {
            total = total-((int)(total*.1));
            return total;
        }
        else
        {
            return total;
        }

    }
}
