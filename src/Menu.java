//NAME: Chris Hinson
//Pitt Program one
//10/9/18

//This class defines all the menuitems, their names, prices, and display prices

public class Menu {

    public Menu()
    {

    }


    private final MenuItem acidPop= new MenuItem("Acid Pop", 12, "12 Knuts");
    private final MenuItem DAacidPop = new MenuItem("Acid Pop (DA Discount)", 11, "11 Knuts");
    private final MenuItem acidPopBag = new MenuItem("Acid Pop (Bag of 5)",58, "2 sickles");
    private final MenuItem DAacidPopBag = new MenuItem("Acid Pop (Bag of 5) (HoneyDuke's Club discount)", 50, "50 Knuts");
    private final MenuItem pumpkinPasties = new MenuItem("Pumpkin Pasties", 116, "4 Sickles");
    private final MenuItem DApumpkinPasties = new MenuItem("Pumpkin Pasties (DA discount)",100, "100 Knuts");
    private final MenuItem everyFlavoredBeansSmall = new MenuItem("Every Flavored Beans (Small Bag)", 50, "50 Knuts");
    private final MenuItem DAeveryFlavoredBeansSmall = new MenuItem("Every Flavored Beans (Small Bag) (HoneyDuke's club discount)",50, "50 Knuts");
    private final MenuItem everyFlavoredBeansLarge = new MenuItem("Every Flavored Beans (Large Bag)", 70, "70 Knuts");
    private final MenuItem DAeveryFlavoredBeansLarge = new MenuItem("Every Flavored Beans (Large Bag)(HoneyDuke's club discount)",58, "2 Sickles");

    public void printDA()
    {
        System.out.println("Here are our items, at HoneyDuke's Club prices");
        System.out.println("Please enter an item Number to add it to your cart");
        System.out.println("1) " + DAacidPop.getName()+ " - " + DAacidPop.getDispCost());
        System.out.println("Note: Acid pops are 50 knuts for a bag of 5, and orders will automatically be rounded to the nearest bag in order to save you money");
        System.out.println("2 )"+DAeveryFlavoredBeansLarge.getName()+ " - " + DAeveryFlavoredBeansLarge.getDispCost());
        System.out.println("3) " +DAeveryFlavoredBeansSmall.getName()+ " - " + DAeveryFlavoredBeansSmall.getDispCost());
        System.out.println("4) " +DApumpkinPasties.getName() + " - " + DApumpkinPasties.getDispCost());
    }

    public void printReg()
    {
        System.out.println("Here are our items");
        System.out.println("Please enter an item Number to add it to your cart");
        System.out.println("1) " +acidPop.getName()+ " - " + acidPop.getDispCost());
        System.out.println("Note: Acid pops are 2 sickles(58 knuts) for a bag of 5, and orders will automatically be rounded to the nearest bag in order to save you money");
        System.out.println("2) " +everyFlavoredBeansLarge.getName()+ " - " + everyFlavoredBeansLarge.getDispCost());
        System.out.println("3) " +everyFlavoredBeansSmall.getName()+ " - " + everyFlavoredBeansSmall.getDispCost());
        System.out.println("4) " +pumpkinPasties.getName() + " - " + pumpkinPasties.getDispCost());
    }

    public MenuItem getAcidPop() {
        return acidPop;
    }

    public MenuItem getDAacidPop() {
        return DAacidPop;
    }

    public MenuItem getAcidPopBag() {return acidPopBag;}

    public MenuItem getDAgetAcidPopBag() {return DAacidPopBag;}

    public MenuItem getPumpkinPasties() {
        return pumpkinPasties;
    }

    public MenuItem getDApumpkinPasties() {
        return DApumpkinPasties;
    }

    public MenuItem getEveryFlavoredBeansSmall() {
        return everyFlavoredBeansSmall;
    }

    public MenuItem getDAeveryFlavoredBeansSmall() {
        return DAeveryFlavoredBeansSmall;
    }

    public MenuItem getEveryFlavoredBeansLarge() {
        return everyFlavoredBeansLarge;
    }

    public MenuItem getDAeveryFlavoredBeansLarge() {
        return DAeveryFlavoredBeansLarge;
    }
}
