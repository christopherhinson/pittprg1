//NAME: Chris Hinson
//Pitt Program one
//10/9/18

//This class is the definition of a customer


public class Customer {

    private String customerName;
    private Bill customerBill = new Bill();
    private Boolean DAMember = Boolean.FALSE;

    public Customer (String name)
    {
        this.customerName = name;
        Bill customerBill = new Bill();
    }


    public String getName()
    {
        return customerName;
    }

    public void setCustomerName(String name)
    {
        name = name;
    }

    public Bill getCustomerBill() {
        return customerBill;
    }

    public void setCustomerBill(Bill bill)
    {
        customerBill = bill;
    }

    public Boolean getDAMember()
    {
        return  DAMember;
    }

    public void setDAMember(Boolean isMember)
    {
        DAMember = isMember;
    }


    public Boolean billIsEmpty()
    {
        return customerBill.isEmpty();
    }

    public void addItem(MenuItem itemAdding, Integer numOfItem)
    {
        if (numOfItem == 0)
        {

        }
        else {
            customerBill.addItem(itemAdding, numOfItem);
        }
    }

    public void removeItem(int itemRemovingIndex, int numOfItemtoRemove)
    {
        customerBill.removeItem(itemRemovingIndex, numOfItemtoRemove);
    }

    public void printCart()
    {
        System.out.println("You currently have:");
        customerBill.print();
        System.out.println(" in your cart.");
    }

    public void printBill()
    {
        System.out.println("Here is your itemized bill:");
        for (int i =0;i<customerBill.getItems().size();i++)
        {
            System.out.println((i+1) + ") " + customerBill.getNumOfItems().get(i) + " " + customerBill.getItems().get(i) + " at " + (customerBill.getCosts().get(i)) + " knuts each");
            System.out.println(customerBill.getCosts().get(i)*customerBill.getNumOfItems().get(i) + " Knuts");
        }

        System.out.println("Here is your total for the day");
        int totalKnutsOwed = customerBill.getTotalOwed(DAMember);
        System.out.println("You owe " + totalKnutsOwed + " knuts");

    }

    public int getTotalOwed()
    {
        return customerBill.getTotalOwed(DAMember);
    }
}
