//NAME: Chris Hinson
//Pitt Program one
//10/9/18

//I made this class to easily handle String checking against a regex

import java.util.regex.*;

public class validate {
     public static Boolean validString(String in, String regex) {
         //defines the regex that the input should be matched to
         Pattern p = Pattern.compile(regex);

         //defines the string that should be being compared to the regex
         Matcher m = p.matcher(in);

         //if statement based on the matcher class matches
         //if the string matches the regex, return true, otherwise, return false
         if (m.matches()) {
             return Boolean.TRUE;
         } else {
             return Boolean.FALSE;
         }
     }

 }
