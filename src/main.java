//NAME: Chris Hinson
//Pitt Program one
//10/9/18

//This is the program's main loop

import java.util.Scanner;

public class main {
    public static void main(String[] args)
    {
        userInput input = new userInput();

        Boolean customerInLine = input.customerInLine();

        while (customerInLine)
        {
            //Create the customer with given name as specified by user
            Customer customer = new Customer(input.getCustomerName());

            //determine whether user is a DA member and set their DAMember flag accordingly
            if (input.getDAMember(customer.getName()))
            {
                customer.setDAMember(Boolean.TRUE);
            }
            else
            {
                customer.setDAMember(Boolean.FALSE);
            }




            Boolean shopping = Boolean.TRUE;

            while (shopping)
            {
                Scanner k = new Scanner(System.in);

                int topMenuChoice = 0;


                System.out.println("Please choose an option \n 1)Add an item to your cart \n 2)Remove an item from your cart \n 3)See items in cart \n 4)Check out ");

                Boolean topMenuChoiceValid = Boolean.FALSE;

                do {

                    if (k.hasNextInt()) {
                        topMenuChoice = k.nextInt();
                        if (topMenuChoice >0 && topMenuChoice < 5)
                        {
                            topMenuChoiceValid = Boolean.TRUE;
                        }
                        else
                        {
                            System.out.println("That's not a valid menu choice please try again");

                        }
                    }
                    else
                    {
                        System.out.println("Please enter the NUMBER of the choice you would like");
                        k.next();
                    }

                }
                while (topMenuChoiceValid == Boolean.FALSE );

                if (topMenuChoice ==1) {
///////////////////////////////////////// ADD ITEM ////////////////////////////////////////////////////////////////////
                    MenuItem itemAdding = input.addItemMenu(customer.getDAMember());
                    int numOfItemtoAdd = input.getNumOfItemtoAdd(itemAdding);

                    if (itemAdding.getName().equals("Acid Pop") || itemAdding.getName().equals("Acid Pop (DA Discount)") && numOfItemtoAdd >=5)
                    {
                        Menu menu = new Menu();
                        int bags = (numOfItemtoAdd/5);
                        int individual = (numOfItemtoAdd%5);

                        if (customer.getDAMember() == Boolean.TRUE)
                        {
                            customer.addItem(menu.getDAgetAcidPopBag(), bags);
                            customer.addItem(menu.getDAacidPop(), individual);
                        }
                        else
                        {
                            customer.addItem(menu.getAcidPopBag(), bags);
                            customer.addItem(menu.getAcidPop(), individual);
                        }
                    }
                    else
                    {
                        customer.addItem(itemAdding, numOfItemtoAdd);
                    }
                }
                else if (topMenuChoice ==2) {
/////////////////////////////////////////////// REMOVE ITEM ////////////////////////////////////////////////////////////
                    if (!customer.billIsEmpty()) {
                        Integer itemToRemoveIndex = input.removeBillItem(customer.getCustomerBill());
                        Integer numOfItemToRemove = input.getNumToRemove(customer.getCustomerBill(), itemToRemoveIndex);
                        customer.getCustomerBill().removeItem(itemToRemoveIndex, numOfItemToRemove);
                    }
                    else
                    {
                        System.out.println("There's nothing in your cart so you cant remove anything from it.");
                    }
                }
                else if (topMenuChoice ==3) {
///////////////////////////////////////////////// PRINT CART ///////////////////////////////////////////////////////////
                    customer.printCart();
                }
                else if (topMenuChoice ==4)
                {
//////////////////////////////////////////// CHECKOUT /////////////////////////////////////////////////////////////////
                    System.out.println("Thank you for shopping with us today!");

                    if (customer.billIsEmpty())
                    {
                        System.out.println("You cant check out with nothing in line, thanks for stopping by though!");
                        break;
                    }


                    customer.printBill();

                    int totalOwed = customer.getTotalOwed();
                    int totalPayed = input.pay(totalOwed);

                    while ((totalPayed-totalOwed)>=24650)
                    {
                        System.out.println("We're sorry, but we only keep 50 galleons at a time to make change, please pay again and be sure to not overpay by more than 50 galleons");
                        totalOwed = customer.getTotalOwed();
                        totalPayed = input.pay(totalOwed);
                    }


                    if (totalPayed>totalOwed)
                    {
                        System.out.println("You are owed change: ");
                        int change = totalPayed-totalOwed;


                        if (change<29)
                        {
                            System.out.println(change + " knuts");
                        }
                        else if (change >29 && change <493)
                        {
                            int sicklesOwed = change/29;
                            int knutsOwed = change%29;

                            if (sicklesOwed != 0)
                            {
                                System.out.println(sicklesOwed + " Sickles");
                            }
                            else if (knutsOwed !=0)
                            {
                                System.out.println(knutsOwed + " Knuts");
                            }
                        }
                        else if (change >493)
                        {
                            int galleonsOwed = change/493;
                            int sicklesOwed = (change - (galleonsOwed*493))/29;
                            int knutsOwed = (change - galleonsOwed*493 - sicklesOwed*29);

                            if (galleonsOwed !=0)
                            {
                                System.out.println(galleonsOwed + " Galleons");
                            }
                            if (sicklesOwed != 0)
                            {
                                System.out.println(sicklesOwed + " Sickles");
                            }
                            if (knutsOwed !=0)
                            {
                                System.out.println(knutsOwed + " Knuts");
                            }
                        }

                    }

                    System.out.println("Thank you for shopping with us today.  Please come again.");



                    shopping = Boolean.FALSE;
                }

            }




            //find out if there is another customer still in line
            customerInLine = input.customerInLine();
        }


    }
}
