//NAME: Chris Hinson
//Pitt Program one
//10/9/18

//This class defines a single item

public class MenuItem {

    private String name;
    private int cost;
    private String dispCost;

    public MenuItem(String name, int cost, String dispCost)
    {
        this.name = name;
        this.cost = cost;
        this.dispCost = dispCost;
    }

    public int getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

    public String getDispCost ()
    {
        return dispCost;
    }
}
